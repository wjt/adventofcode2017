#!/usr/bin/env python3
import argparse
import numpy as np


def solve(digits, roll_by):
    ints = np.array([int(x) for x in digits])
    roll = np.roll(ints, roll_by)
    return ints[ints == roll].sum()



def main():
    p = argparse.ArgumentParser()
    p.add_argument('--part2', action='store_true')
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    digits = a.input.read().strip()
    roll_by = len(digits) // 2 if a.part2 else 1

    print(solve(digits, roll_by))


if __name__ == '__main__':
    main()
