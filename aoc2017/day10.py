#!/usr/bin/env python3
import argparse
import numpy as np


def solve(lengths, array_size, rounds):
    array = np.arange(array_size, dtype='uint8')
    pos = 0
    skip = 0
    for _ in range(rounds):
        for length in lengths:
            ix = np.arange(pos, pos + length) % array_size
            array[ix] = array[ix][::-1]
            pos += length + skip
            skip += 1
            # print(length, array)

    return array


def knot_hash(lengths):
    lengths += [17, 31, 73, 47, 23]
    array = solve(lengths, array_size=256, rounds=64)
    # https://stackoverflow.com/questions/20553551
    # This language is crap, huh?
    # pylint: disable=E1101
    dense = np.bitwise_xor.reduce(array.reshape((16, 16)), axis=1)
    return dense


def knot_hex(lengths):
    dense = knot_hash(lengths)
    return ''.join(map('{:02x}'.format, dense))


def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--array-size', type=int, default=256)
    group.add_argument('--part2', action='store_true')
    parser.add_argument('input', type=argparse.FileType('r'))
    args = parser.parse_args()

    if args.part2:
        lengths = list(args.input.read().strip().encode('ascii'))
        print(knot_hex(lengths))
    else:
        lengths = [int(x) for x in args.input.read().strip().split(',')]
        array = solve(lengths, args.array_size, rounds=1)
        print(int(array[0]) * array[1])


if __name__ == '__main__':
    main()
