#!/usr/bin/env python3
import argparse


DIRECTIONS = {
    'n': 0 + 1j,
    'ne': 1 + .5j,
    'se': 1 - .5j,
    's': 0 - 1j,
    'sw': -1 - .5j,
    'nw': -1 + .5j,
}


def move(directions):
    position = 0 + 0j
    furthest_point = 0
    for direction in directions.split(','):
        position += DIRECTIONS[direction]
        furthest_point = max(furthest_point, hexhattan(position))
    return position, furthest_point


def hexhattan(position):
    '''
    Intuition: get to the correct column, then move vertically.
    - If x < y * 2, move in a diagonal straight line, then vertically.
    - Otherwise, move in a diagonal straight line to y=0, then zig-zag
      between y=0 and y=.5
    '''
    x, y = map(abs, (position.real, position.imag))
    z = min(x, y * 2)
    x -= z
    y -= (z / 2)
    return z + x + y


def solve(directions):
    position, furthest_point = move(directions)
    return position, furthest_point, hexhattan(position)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('r'))
    args = parser.parse_args()

    directions = args.input.read().strip()
    print(solve(directions))


if __name__ == '__main__':
    main()
