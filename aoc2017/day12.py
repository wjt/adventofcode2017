#!/usr/bin/env python3
import argparse
import collections


def parse(lines):
    graph = {}
    for line in lines:
        x, ys = line.strip().split(' <-> ')
        graph[x] = frozenset(ys.split(', '))
    return graph


def reachable(graph, origin):
    visited = set()
    queue = collections.deque([origin])
    while queue:
        x = queue.popleft()
        if x in visited:
            continue
        visited.add(x)
        queue.extend(graph[x])

    return visited

def solve(lines):
    graph = parse(lines)
    part1 = len(reachable(graph, '0'))

    visited = set()
    n_subgraphs = 0
    for origin in graph:
        if origin in visited:
            continue
        visited |= reachable(graph, origin)
        n_subgraphs += 1

    return part1, n_subgraphs

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('r'))
    args = parser.parse_args()

    print(solve(args.input))


if __name__ == '__main__':
    main()
