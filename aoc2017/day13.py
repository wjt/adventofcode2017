#!/usr/bin/env python3
import argparse
import itertools


def parse(lines):
    data = []
    for line in lines:
        depth, r = map(int, line.strip().split(': '))
        period = 2 * (r - 1)
        data.append((depth, r, period))
    return data


def part1(data):
    severity = 0
    for depth, r, period in data:
        if depth % period == 0:
            severity += depth * r
    return severity


def part2(data):
    '''Smells a bit Chinese Remainder Theorem-y except we want:

        offset + depth ≠ 0 (mod r)

    So I tried brute-force first and it only takes 2 seconds.'''
    for offset in itertools.count():
        caught = False
        for depth, _, period in data:
            if (offset + depth) % period == 0:
                caught = True
                break
        if not caught:
            return offset


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('r'))
    parser.add_argument('--part2', action='store_true')
    args = parser.parse_args()

    print((part2 if args.part2 else part1)(parse(args.input)))


if __name__ == '__main__':
    main()
