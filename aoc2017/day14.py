#!/usr/bin/env python3
import argparse
import collections
import numpy as np

from .day10 import knot_hash


def neighbours(ix):
    x, y = ix
    if x > 0:
        yield x - 1, y
    if x < 127:
        yield x + 1, y
    if y > 0:
        yield x, y - 1
    if y < 127:
        yield x, y + 1


def flood_fill(grid, regions, ix, label):
    q = collections.deque([ix])
    while q:
        ix_ = q.popleft()
        if grid[ix_] and regions[ix_] == 0:
            regions[ix_] = label
            q.extend(neighbours(ix_))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('key')
    args = parser.parse_args()

    key = args.key.encode('ascii')
    pattern = key + b'-%d'
    grid = np.vstack(
        np.unpackbits(knot_hash(list(pattern % i)))
        for i in range(128)
    )
    print(grid[:8, :8])
    print(np.count_nonzero(grid))

    # ha! could have used
    # https://docs.scipy.org/doc/scipy-0.16.0/reference/generated/scipy.ndimage.measurements.label.html

    regions = np.zeros(shape=grid.shape, dtype='uint64')
    n_regions = 0
    for ix in map(tuple, np.argwhere(grid != 0)):
        if regions[ix] == 0:
            n_regions += 1
            flood_fill(grid, regions, ix, n_regions)

    print(regions[:8, :8])
    print(n_regions)


if __name__ == '__main__':
    main()
