#!/usr/bin/env pypy3
# PyPy is an order of magnitude faster
# https://en.wikipedia.org/wiki/Lehmer_random_number_generator
import argparse
import itertools


def lehmer(seed, factor):
    modulus = 2147483647  # 2 ** 31 - 1
    val = seed
    while True:
        val = (val * factor) % modulus
        yield val


def lehmer_filter(seed, factor, filter_):
    modulus = 2147483647  # 2 ** 31 - 1
    val = seed
    while True:
        val = (val * factor) % modulus
        if val % filter_ == 0:
            yield val


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--rounds', type=int)
    parser.add_argument('--part2', action='store_true')
    parser.add_argument('a', type=int)
    parser.add_argument('b', type=int)
    args = parser.parse_args()

    a_seed = 16807
    b_seed = 48271

    rounds = args.rounds or (5000000 if args.part2 else 40000000)
    if args.part2:
        a = lehmer_filter(args.a, a_seed, 4)
        b = lehmer_filter(args.b, b_seed, 8)
    else:
        a = lehmer(args.a, a_seed)
        b = lehmer(args.b, b_seed)

    total = 0
    MASK = 2 ** 16 - 1
    for (x, y) in itertools.islice(zip(a, b), rounds):
        if (x & MASK) == (y & MASK):
            total += 1
    print(total)


if __name__ == '__main__':
    main()
