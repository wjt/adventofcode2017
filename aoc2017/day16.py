#!/usr/bin/env pypy3
import argparse


class State(object):
    def __init__(self, length):
        self.array = list(range(length))
        self.offset = 0
        self.length = length

    def s(self, i):
        # Wildly premature optimisation
        self.offset += i

    def x(self, i, j):
        i_ = (i - self.offset) % self.length
        j_ = (j - self.offset) % self.length
        self.array[i_], self.array[j_] = self.array[j_], self.array[i_]

    def p(self, a_, b_):
        i = self.array.index(a_)
        j = self.array.index(b_)
        self.array[i], self.array[j] = self.array[j], self.array[i]

    def show(self):
        return ''.join(
            chr(self.array[(i - self.offset) % self.length] + ord('a'))
            for i in range(self.length)
        )


def s(rest):
    i = int(rest)
    return lambda state: state.s(i)


def x(rest):
    i, j = map(int, rest.split('/'))
    return lambda state: state.x(i, j)


def p(rest):
    a, b = rest.split('/')
    a_ = ord(a) - ord('a')
    b_ = ord(b) - ord('a')
    return lambda state: state.p(a_, b_)


def compile_dance(commands):
    generators = {'s': s, 'x': x, 'p': p}
    lines = []
    for command in commands:
        c = command[0]
        rest = command[1:]
        lines.append(generators[c](rest))
    return lines


def go(commands, length, rounds):
    state = State(length)
    dance = compile_dance(commands)
    seen = []

    seen.append(state.show())
    for i in range(rounds):
        for f in dance:
            f(state)

        new = state.show()
        try:
            j = seen.index(new)
        except ValueError:
            seen.append(new)
        else:
            assert j == 0
            period = i + 1
            return period, seen[rounds % period]

    return state.show()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--length', type=int, default=16)
    parser.add_argument('--rounds', type=int, default=1)
    parser.add_argument('input', type=argparse.FileType('r'))
    args = parser.parse_args()

    print(go(args.input.read().strip().split(','), args.length, args.rounds))


if __name__ == '__main__':
    main()
