#!/usr/bin/env pypy3
import argparse


class Cell(object):
    '''The cool thing about this is I traded O(n) inserts for O(n) indexing
    and the overhead is almost certainly higher. It also made part 2 less
    obvious. Think before you type!'''
    def __init__(self, data, succ=None):
        self.data = data
        self.succ = succ or self

    def __iter__(self):
        yield self
        node = self.succ
        while node != self:
            yield node
            node = node.succ

def part1(steps, rounds):
    zero = head = Cell(0)

    def show():
        print(''.join(
            ('({})' if node == head else ' {} ').format(node.data)
            for node in zero
        ))

    # show()
    for j in range(1, rounds + 1):
        for k in range(steps):
            head = head.succ
        head.succ = Cell(j, head.succ)
        head = head.succ

    # show()
    return head.succ.data


def part2(steps):
    pos = 0
    n = 1
    ret = None
    for i in range(1, 50000001):
        pos = (pos + steps) % n + 1
        if pos == 1:
            ret = i
        n += 1
    return ret


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('steps', type=int)
    args = parser.parse_args()

    print(part1(args.steps, 2017))
    print(part2(args.steps))


if __name__ == '__main__':
    main()
