#!/usr/bin/env python3
import argparse
from collections import deque


def compile_arg(arg):
    try:
        return int(arg)
    except ValueError:
        return arg


class Recovered(Exception):
    pass


class Machine(object):
    def __init__(self, name, lines):
        self.name = name
        self.r = {x: 0 for x in "abcdefghijklmnopqrstuvwxyz"}
        self.pc = 0
        self.instructions = []
        self.freq = None
        for i, line in enumerate(lines):
            args = line.split('#')[0].split()
            inst = args.pop(0)
            f = tuple(
                [getattr(self.__class__, inst)] +
                [compile_arg(a) for a in args])
            self.instructions.append(f)
        self.n = len(self.instructions)

    def val(self, x):
        if isinstance(x, str):
            return self.r[x]
        elif isinstance(x, int):
            return x
        else:
            raise ValueError(x)

    def set(self, x, y):
        self.r[x] = self.val(y)
        self.pc += 1

    def add(self, x, y):
        self.r[x] += self.val(y)
        self.pc += 1

    def mul(self, x, y):
        self.r[x] *= self.val(y)
        self.pc += 1

    def mod(self, x, y):
        self.r[x] %= self.val(y)
        self.pc += 1

    def jgz(self, x, y):
        x_ = self.val(x)
        if x_ > 0:
            y_ = self.val(y)
            self.pc += y_
        else:
            self.pc += 1

    def run_one(self):
        f, *args = self.instructions[self.pc]
        self._list_one(self.pc, f, args)
        f(self, *args)

    def run(self):
        while self.runnable():
            self.run_one()

    def runnable(self):
        return 0 <= self.pc < self.n

    def _list_one(self, i, f, args):
        print(('%s %3d %s' + (' %3s' * len(args))) % (
            (self.name, i, f.__name__) + tuple(args)))


class Part1(Machine):
    def __init__(self, lines):
        super(Part1, self).__init__('-', lines)
        self.freq = None

    def snd(self, x):
        self.freq = self.val(x)
        self.pc += 1

    def rcv(self, x):
        if self.val(x) != 0:
            raise Recovered(self.freq)
        self.pc += 1


class Part2(Machine):
    def __init__(self, lines, p, inq, outq):
        super(Part2, self).__init__(str(p), lines)
        self.r['p'] = p
        self.inq = inq
        self.outq = outq
        self.blocked = False
        self.sends = 0

    def snd(self, x):
        self.outq.append(self.val(x))
        self.sends += 1
        self.pc += 1

    def rcv(self, x):
        if self.inq:
            self.r[x] = self.inq.popleft()
            self.pc += 1
            self.blocked = False
        else:
            self.blocked = True

    def runnable(self):
        if not super(Part2, self).runnable():
            return False

        f = self.instructions[self.pc][0]
        return f.__name__ != 'rcv' or bool(self.inq)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--part2', action='store_true')
    p.add_argument('source', type=argparse.FileType('r'))
    a = p.parse_args()
    lines = tuple(a.source)

    if not a.part2:
        part1 = Part1(lines)
        try:
            part1.run()
        except Recovered as e:
            print(e)
        else:
            raise ValueError
    else:
        q0 = deque()
        q1 = deque()

        part2_0 = Part2(lines, 0, q0, q1)
        part2_1 = Part2(lines, 1, q1, q0)

        while part2_0.runnable() or part2_1.runnable():
            part2_0.run()
            part2_1.run()

        print(part2_0.sends, part2_1.sends)


if __name__ == '__main__':
    main()
