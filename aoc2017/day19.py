#!/usr/bin/env python3
import argparse
import itertools
import attr


@attr.s
class Coord:
    row = attr.ib()
    col = attr.ib()

    def __add__(ij, kl):
        return Coord(ij.row + kl.row, ij.col + kl.col)

    def perps(self):
        assert abs(self.row) + abs(self.col) == 1
        yield Coord(row=self.col, col=self.row)
        yield Coord(row=-self.col, col=-self.row)


class Grid:
    def __init__(self, text):
        self.rows = text.split('\n')
        self.m = len(self.rows)
        self.n = max(map(len, self.rows))
        self.origin = Coord(0, self.rows[0].index('|'))

    def __getitem__(self, coord):
        try:
            return self.rows[coord.row][coord.col]
        except IndexError:
            return ' '


def walk(grid):
    pos = grid.origin
    direction = Coord(row=1, col=0)
    seen = []

    for i in itertools.count():
        c = grid[pos]
        if c == ' ':
            break
        if c.isupper():
            seen.append(c)
        if c == '+':
            for perp in direction.perps():
                if grid[pos + perp] != ' ':
                    direction = perp
                    break
        pos += direction

    return ''.join(seen), i


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--part2', action='store_true')
    p.add_argument('source', type=argparse.FileType('r'))
    args = p.parse_args()
    grid = Grid(args.source.read())

    print(walk(grid))


if __name__ == '__main__':
    main()
