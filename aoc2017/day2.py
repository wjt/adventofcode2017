#!/usr/bin/env python3
import argparse
import itertools


def checksum(spreadsheet):
    return sum(max(row) - min(row) for row in spreadsheet)


def part2(spreadsheet):
    x = 0
    for row in spreadsheet:
        for y, z in itertools.combinations(sorted(row), 2):
            q, r = divmod(z, y)
            if r == 0:
                x += q
                break
        else:
            raise ValueError(row)
    return x


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--part2', action='store_true')
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    spreadsheet = [
        [int(x) for x in line.split()]
        for line in a.input
    ]
    print((part2 if a.part2 else checksum)(spreadsheet))


if __name__ == '__main__':
    main()
