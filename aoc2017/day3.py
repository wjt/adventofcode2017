#!/usr/bin/env python3
import math
import requests

def steps(z):
    q = math.ceil(math.sqrt(z))
    q += (q + 1) % 2
    b = q ** 2
    # b is next largest odd square; q is that odd
    corners = [b - i * (q - 1) for i in range(4)]
    dist_from_corner = min(abs(z - corner) for corner in corners)
    return (q - 1) - dist_from_corner


def part2(z):
    '''Sorry, can't be bothered writing this tedious spiral code'''
    r = requests.get('http://oeis.org/A141481/b141481.txt')
    for row in r.text.split('\n'):
        if row.startswith('#'):
            continue
        n, a_n = map(int, row.split())
        if a_n > z:
            return a_n
