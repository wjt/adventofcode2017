#!/usr/bin/env python3
import argparse
import collections
import itertools


def valid(line, anagrams):
    c = collections.Counter()
    for word in line.strip().split():
        if anagrams:
            word = ''.join(sorted(word))
        c[word] += 1
    [(w, n)] = c.most_common(1)
    is_valid = n == 1
    return is_valid


def solve(lines, anagrams):
    return sum(valid(line, anagrams) for line in lines)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--part2', action='store_true')
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    print(solve(a.input, a.part2))


if __name__ == '__main__':
    main()
