#!/usr/bin/env python3
import argparse


def steps(offsets, weird):
    i = 0
    n = 0
    k = len(offsets)
    while 0 <= i < k:
        j = offsets[i]
        offsets[i] += -1 if (weird and j >= 3) else 1
        i += j
        n += 1

    return n


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--part2', action='store_true')
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    offsets = [int(x) for x in a.input]

    print(steps(offsets, a.part2))


if __name__ == '__main__':
    main()
