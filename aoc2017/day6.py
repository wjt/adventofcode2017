#!/usr/bin/env python3
import argparse


def find_cycle(banks):
    seen = {}
    n = len(banks)
    while banks not in seen:
        seen[banks] = len(seen)
        scratch = list(banks)
        k = max(scratch)
        i = scratch.index(k)
        scratch[i] = 0
        for j in range(i + 1, i + k + 1):
            scratch[j % n] += 1
        banks = tuple(scratch)

    return len(seen), len(seen) - seen[banks]


def main():
    p = argparse.ArgumentParser()
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    banks = tuple(int(x) for x in a.input.read().split())

    print(find_cycle(banks))


if __name__ == '__main__':
    main()
