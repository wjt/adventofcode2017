#!/usr/bin/env python3
'''The shore'''
import argparse
import attr
import re


class RequiredWeight(Exception):
    '''I can see the shore from here'''
    pass


@attr.s
class Node(object):
    '''I can see your town, I see your house, and you'''
    name = attr.ib()
    weight = attr.ib()
    children = attr.ib()


@attr.s
class Tree(object):
    '''The scores'''
    root = attr.ib()
    nodes = attr.ib()

    def weigh(self, name):
        '''I count the letters of your name.'''
        node = self.nodes[name]
        child_weights = {}
        weight_children = {}
        for name_ in node.children:
            weight = self.weigh(name_)
            child_weights[name_] = weight
            weight_children.setdefault(weight, set()).add(name_)

        if len(weight_children) > 1:
            odd, rest = sorted(weight_children.items(),
                               key=lambda t: len(t[1]))
            odd_weight, (odd_name,) = odd
            rest_weight, _ = rest
            raise RequiredWeight(odd_name,
                                 self.nodes[odd_name].weight -
                                 (odd_weight - rest_weight))

        return node.weight + sum(child_weights.values())

    def find_odd_weight(self):
        '''I count the days till you are here, again.'''
        try:
            self.weigh(self.root)
        except RequiredWeight as e:
            return e.args
        else:
            assert False


def parse(lines):
    '''Day 7'''
    nodes = {}
    has_parent = set()

    pattern = re.compile(r'^(\w+) \((\d+)\)(?: -> (.*))?$')
    for line in lines:
        m = pattern.match(line.strip())
        name, weight, children = m.groups()
        if children is not None:
            children = children.split(', ')
        else:
            children = []

        has_parent.update(children)
        nodes[name] = Node(name, int(weight), children)

    root, = nodes.keys() - has_parent
    return Tree(root, nodes)


def main():
    '''And I'm love galore'''
    p = argparse.ArgumentParser()
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    tree = parse(a.input)
    print(tree.root)
    print(tree.find_odd_weight())


if __name__ == '__main__':
    main()
