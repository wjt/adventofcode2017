#!/usr/bin/env python3
import argparse
import collections
import operator

operators = {
    'inc': 1,
    'dec': -1,
}

comparisons = {
    '>': operator.gt,
    '>=': operator.ge,
    '<': operator.lt,
    '<=': operator.le,
    '==': operator.eq,
    '!=': operator.ne,
}


def interpret(lines):
    registers = collections.defaultdict(int)
    ever = 0
    for line in lines:
        r1, op, v1, _if, r2, c, v2 = line.strip().split()
        if comparisons[c](registers[r2], int(v2)):
            registers[r1] += (operators[op] * int(v1))
            ever = max(ever, registers[r1])
    return max(registers.values()), ever


def main():
    p = argparse.ArgumentParser()
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    print(*interpret(a.input))


if __name__ == '__main__':
    main()
