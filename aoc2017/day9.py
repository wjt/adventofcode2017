#!/usr/bin/env python3
import argparse


def interpret(data):
    i = 0
    n = len(data)
    depth = 0
    groups = 0
    total_score = 0
    in_garbage = False
    garbage_count = 0
    while i < n:
        c = data[i]
        i += 1
        if c == '!':
            i += 1
        elif in_garbage:
            if c == '>':
                in_garbage = False
            else:
                garbage_count += 1
        elif c == '<':
            in_garbage = True
        elif c == '{':
            groups += 1
            depth += 1
            total_score += depth
        elif c == '}':
            depth -= 1
    assert depth == 0
    assert not in_garbage
    return groups, total_score, garbage_count


def main():
    p = argparse.ArgumentParser()
    p.add_argument('input', type=argparse.FileType('r'))
    a = p.parse_args()

    print(*interpret(a.input.read().strip()))


if __name__ == '__main__':
    main()
