import pytest
from .day11 import solve


@pytest.mark.parametrize('directions,distance', [
    ('ne,ne,ne', 3),
    ('ne,ne,sw,sw', 0),
    ('ne,ne,s,s', 2),
    ('se,sw,se,sw,sw', 3),
    ('se,ne,se,ne', 4),
])
def test_solve(directions, distance):
    assert solve(directions)[-1] == distance
