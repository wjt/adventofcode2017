import os
import pytest
from . import day16


def test_part1():
    path = os.path.join(os.path.dirname(__file__), '..', 'input', 'day16.txt')
    with open(path) as f:
        source = f.read().strip().split(',')

    assert day16.go(source, 16, 1) == 'hmefajngplkidocb'
