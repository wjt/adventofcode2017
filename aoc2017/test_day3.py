import pytest
from .day3 import steps


@pytest.mark.parametrize('z,distance', [
    (1, 0),
    (2, 1),
    (12, 3),
    (23, 2),
    (1024, 31),
])
def test_distance(z, distance):
    assert steps(z) == distance
