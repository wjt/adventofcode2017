import pytest
from .day5 import steps


@pytest.mark.parametrize('offsets,weird,n', [
    ([0, 3,  0,  1,  -3], False, 5),
    ([0, 3,  0,  1,  -3], True, 10),
])
def test_distance(offsets, weird, n):
    assert steps(offsets, weird) == n
