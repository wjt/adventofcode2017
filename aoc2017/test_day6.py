import pytest
from .day6 import find_cycle


@pytest.mark.parametrize('banks,steps,loop_length', [
    ((0, 2, 7, 0), 5, 4),
])
def test_distance(banks, steps, loop_length):
    assert find_cycle(banks) == (steps, loop_length)
