import pytest
from .day9 import interpret


@pytest.mark.parametrize('data,groups,total_score', [
    ('{}', 1, 1),
    ('{{{}}}', 3, 6),
    ('{{},{}}', 3, 5),
    ('{{{},{},{{}}}}', 6, 16),
    ('{<{},{},{{}}>}', 1, 1),
    ('{<a>,<a>,<a>,<a>}', 1, 1),
    ('{{<a>},{<a>},{<a>},{<a>}}', 5, 9),
    ('{{<!>},{<!>},{<!>},{<a>}}', 2, 3),
])
def test_interpret(data, groups, total_score):
    assert interpret(data)[0:2] == (groups, total_score)
